import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

public class BaseDeDatos {

	
	public static void agregarALaBD(String Nombre, int cantidad, Double precio)
			throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = " insert into productos (nombre, cantidad, precio)" + " values (?, ?, ?)";

			// insert mediante preparedStatement donde doy como par�metro el query y los
			// valores del objeto cliente
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, Nombre);
			preparedStmt.setInt(2, cantidad);
			preparedStmt.setDouble(3, precio);

			// Ejecuto el preparedStatemen, de esta manera, inserto los valores a la base de
			// datos
			preparedStmt.executeUpdate();
			// cierro la conexi�n.
			conn.close();
		} catch (SQLException se) {
			// log the exception
			throw se;
		}
	}
	
	public static void updateproducto(int id, String Nombre, int cantidad, Double precio)
			throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			// Query de inserci�n de datos en la tabla cliente
			String query = "UPDATE productos SET nombre = ?, cantidad = ?, precio = ?  WHERE id = ?";
			// String query = "UPDATE cliente SET nombre = 'popo', apellido = 'asdf',
			// mail='fdsa' WHERE id = 31";

			// create our java preparedstatement using a sql update query
			PreparedStatement ps = conn.prepareStatement(query);

			// set the preparedstatement parameters

			ps.setString(1, Nombre);
			ps.setInt(2, cantidad);
			ps.setDouble(3, precio);
			ps.setInt(4, id);

			// call executeUpdate to execute our sql update statement
			ps.executeUpdate();
			ps.close();
		} catch (SQLException | ClassNotFoundException e) {
			// log the exception
			System.err.println("Se ha generado la siguiente excepci�n:");
			System.err.println(e.getMessage());

		}
	}
	
	// a java preparedstatement example
	//String Nombre, String apellido, String mail, Float Monto, int DNI, int nroCuenta
	public static void agregarALaBD(String Nombre, String apellido, String mail, float monto, int dni, int nroCuenta)
			throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/ejercicioclientes";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			String query = " insert into cliente (nombre, apellido, mail, monto, dni, nroCuenta)" + " values (?, ?, ?, ?, ?, ?)";

			// insert mediante preparedStatement donde doy como par�metro el query y los
			// valores del objeto cliente
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString(1, Nombre);
			preparedStmt.setString(2, apellido);
			preparedStmt.setString(3, mail);
			preparedStmt.setFloat(4, monto);
			preparedStmt.setInt(5, dni);
			preparedStmt.setInt(6, nroCuenta);
			// Ejecuto el preparedStatemen, de esta manera, inserto los valores a la base de
			// datos
			preparedStmt.executeUpdate();
			// cierro la conexi�n.
			conn.close();
			System.out.println("Se agrego el cliente: " + Nombre);
		} catch (SQLException se) {
			// log the exception
			throw se;
		}
	}

	public static void updatecliente(int id, String Nombre, String apellido, String mail)
			throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = (Connection) DriverManager.getConnection(myUrl, "root", "");
			// Query de inserci�n de datos en la tabla cliente
			String query = "UPDATE cliente SET nombre = ?, apellido = ?, mail = ?  WHERE id = ?";
			// String query = "UPDATE cliente SET nombre = 'popo', apellido = 'asdf',
			// mail='fdsa' WHERE id = 31";

			// create our java preparedstatement using a sql update query
			PreparedStatement ps = conn.prepareStatement(query);

			// set the preparedstatement parameters

			ps.setString(1, Nombre);
			ps.setString(2, apellido);
			ps.setString(3, mail);
			ps.setInt(4, id);

			// call executeUpdate to execute our sql update statement
			ps.executeUpdate();
			ps.close();
		} catch (SQLException | ClassNotFoundException e) {
			// log the exception
			System.err.println("Se ha generado la siguiente excepci�n:");
			System.err.println(e.getMessage());

		}
	}

	public static void borrarTablaDeLaBaseDeDatos(String tabla) throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			Statement stmt;
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = DriverManager.getConnection(myUrl, "root", "");

			String sql = "DELETE FROM " + tabla;
			PreparedStatement prest = conn.prepareStatement(sql);
			prest.executeUpdate();
			conn.close();

		} catch (SQLException s) {
			System.out.println("SQL statement is not executed!");
		}
	}
	
	public static void bajaRegistro(String tabla, int id) throws SQLException, ClassNotFoundException {
		try {
			// Creo la conexi�n a la base de datos dando como par�metro el String de
			// conexi�n y el Driver
			Statement stmt;
			String myDriver = "org.gjt.mm.mysql.Driver";
			String myUrl = "jdbc:mysql://localhost/abm";
			Class.forName(myDriver);
			// Doy los par�metros necesarios para la conexi�n con la BD, usuario y
			// contrase�a
			Connection conn = DriverManager.getConnection(myUrl, "root", "");

			String sql = "DELETE FROM " + tabla + " WHERE id = " + id;
			PreparedStatement prest = conn.prepareStatement(sql);
			prest.executeUpdate();
			conn.close();
			System.out.println("Borro el id: " + id + " de la tabla " + tabla);

		} catch (SQLException s) {
			System.out.println("SQL statement is not executed!");
		}
	}
	
	

}

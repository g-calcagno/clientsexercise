import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

public class Ejecutable {

	public static void main(String[] args)
			throws SQLException, ClassNotFoundException, InterruptedException, IOException {

		BaseDeDatos bd = new BaseDeDatos();
		// bd.borrarTablaDeLaBaseDeDatos();

		/*
		Cliente cliente = new Cliente("cliente", "apellido", "cliente@gmail.com");
		Cliente cliente2 = new Cliente("cliente2", "apellido2", "cliente2@gmail.com");
		Cliente cliente3 = new Cliente("cliente3", "apellido3", "cliente3@gmail.com");

		bd.agregarALaBD(cliente.getNombre(), cliente.getApellido(), cliente.getMail());
		bd.agregarALaBD(cliente2.getNombre(), cliente2.getApellido(), cliente3.getMail());
		bd.agregarALaBD(cliente3.getNombre(), cliente3.getApellido(), cliente3.getMail());
		bd.updatecliente(55, "cambio", "otro", "asdf2gmail.com");
		*/

/*
		bd.agregarALaBD(p1.getNombre(), p1.getCantidad(), p1.getPrecio());
		bd.agregarALaBD(p2.getNombre(), p2.getCantidad(), p2.getPrecio());
		bd.agregarALaBD(p3.getNombre(), p3.getCantidad(), p3.getPrecio());
		bd.updateproducto(10, "Amasadora Cambiada", 12, 16.700);
*/
		
		int id, dato, dato3;
		float dato2;
		String tabla = "cliente";
		String[] sWhatever = new String[7];
		boolean flag = true;
		Scanner scanIn = new Scanner(System.in);
		// List<String> sWhatever = new ArrayList<String>();
		while (flag == true) {
			System.out.print("Ingrese el nombre del cliente: ");
			
			sWhatever[0] = scanIn.nextLine();
			System.out.print("\nIngrese el apellido: ");
			if (scanIn.hasNextLine()) {
				sWhatever[1] = scanIn.nextLine();
			}

			System.out.print("\nIngrese el mail: ");
			if (scanIn.hasNextLine()) {
				sWhatever[2] = scanIn.nextLine();
			}
			
			System.out.print("\nIngrese el monto: ");
			if (scanIn.hasNextLine()) {
				sWhatever[3] = scanIn.nextLine();
			}
			
			System.out.print("\nIngrese el DNI: ");
			if (scanIn.hasNextLine()) {
				sWhatever[4] = scanIn.nextLine();
			}
			
			System.out.print("\nIngrese el numero de cuenta: ");
			if (scanIn.hasNextLine()) {
				sWhatever[5] = scanIn.nextLine();
			}
			
			dato = Integer.parseInt(sWhatever[4]);	//dni
			dato2 = Float.parseFloat(sWhatever[3]);	//monto
			dato3 = Integer.parseInt(sWhatever[5]);	//nro cuenta
			
			bd.agregarALaBD(sWhatever[0],sWhatever[1],sWhatever[2],dato2,dato,dato3);
			
			System.out.println("\nDesea agregar otro cliente operación? (y/n): ");
			if (scanIn.hasNextLine()) {
				sWhatever[6] = scanIn.nextLine();
			}

			sWhatever[6] = sWhatever[6].toLowerCase();
			String regex = "n";
			if (sWhatever[6].matches(regex)) {
				flag = false;
				scanIn.close();
			}
		}
		System.out.println("Programa finalizado.");
		System.exit(0);
		
	}
}

public class Cliente {
	private String nombre;
	private String apellido;
	private String mail;
	private int monto;
	private int dni,nroCuenta;
	
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cliente(String nombre, String apellido, String mail, int monto, int dni, int nroCuenta) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.monto = monto;
		this.dni = dni;
		this.nroCuenta = nroCuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setMonto(int monto) {
		this.monto = monto;
	}
	
	public int getMonto() {
		return monto;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}
	
	public float getDni() {
		return dni;
	}
	
	public void setNroCuenta(int nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	
	public float getNroCuenta() {
		return nroCuenta;
	}
	
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

}